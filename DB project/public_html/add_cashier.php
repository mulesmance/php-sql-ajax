<html>
<head>
	<title>Add cashier</title>
	<link rel = "stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="formbox">
<form action="addcashier.php" method=post>
	<p>
	  <label for="cid" id="preinput"> IDENTIFICATION NUMBER : </label>
	  <input type="text" name="cid" required placeholder="Enter the id number" id="inputid"/>
	</p>

	<p>
	  <label  for="first_name" id="preinput"> CASHIER FNAME : </label>
	  <input type="text" name="first_name" required placeholder="Enter the first name of the cashier" id="inputid" />
	</p>

        <p>
	  <label  for="last_name" id="preinput"> CASHIER LNAME : </label>
	  <input type="text" name="last_name" required placeholder="Enter the last name of the cashier" id="inputid" />
	</p>

	<p>
	  <label  id="preinput"> CASHIER ADDRESS : </label>
	  <input type="text" name="address"  id="inputid" />
	</p>

	<p>
	  <label id="preinput"> CITY LIVED : </label>
	  <input type="text" name="city"  id="inputid" />
	</p>

        <p>
	  <label id="preinput"> POSTAL CODE : </label>
	  <input type="text" name="postal_code"  id="inputid" />
	</p>

	<p>
	  <label id="preinput"> CASHIER EMAIL : </label>
	  <input type="text" name="email" id="inputid" />
	</p>

	<p>
	  <label id="preinput"> PHONE NUMBER : </label>
	  <input type="text" name="phone_number"  id="inputid" />
	</p>

	<p>
	  <label for="sin_number" id="preinput"> SIN NUMBER : </label>
	  <input type="text" name="sin_number"  required placeholder="Enter the sin # of the cashier" id="inputid" />
	</p>
	<p>
	  <input type="submit" name="send" value="Submit" id="inputid1"  />
	</p>
</div>
</body>
</html>
